let path = require('path');
let cors = require('cors');
let logger = require('morgan');
let express = require('express');
let bodyParser = require('body-parser');
const port = process.env.PORT || 3000;

let app = express();

var allowedOrigins = ['http://localhost:8080', 'https://shielded-beyond-24745.herokuapp.com'];
app.use(cors({
  origin: function(origin, callback) {
    if (!origin) return callback(null, true);
    if (allowedOrigins.indexOf(origin) === -1) {
      var msg = 'The CORS policy for this site does not allow access from the specified Origin.';
      return callback(console.log(msg), false);
    }
    return callback(null, true);
  }
}));
app.use(force_https);
app.use(express.static(path.join(__dirname, 'dist')));
app.disable('x-powered-by');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// app.use('/', require('./routes/index'));
app.use('/video', require('./routes/video'));
app.use(function(req, res, next) {
	let err = new Error('Not Found');
		err.status = 404;
	next(err);
});

app.use(function(err, req, res, next) {
	let obj_message = {
		message: err.message
	};
	if(process.env.NODE_ENV == 'development')
	{
		obj_message.error = err;
		console.error(err);
	}
	res.status(err.status || 500);
	res.json(obj_message);
});

// Helpers 

function force_https(req, res, next) {
	if (process.env.NODE_ENV == 'production') {
		if (req.headers['x-forwarded-proto'] !== 'https') {
			return res.redirect('https://' + req.get('host') + req.url);
		}
	}
	next();
}
app.listen(port, () => {
	console.log(`listening on ${port}`);
});
